<?php

namespace Drupal\Tests\yaml_translation_import\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\yaml_translation_import\Utility\Flatten;

/**
 * Tests \Drupal\yaml_translation_import\Utility\Flatten.
 *
 * @group yaml_translation_import
 *
 * @coversDefaultClass \Drupal\yaml_translation_import\Utility\Flatten
 */
class FlattenTest extends UnitTestCase {

  /**
   * Tests the the ::transform method.
   *
   * @dataProvider flattenDataProvider
   *
   * @covers ::transform
   */
  public function testFlatten(array $messages, array $result) {
    Flatten::transform($messages);
    $this->assertEquals($result, $messages);
  }

  /**
   * Test the flatten transform method.
   *
   * @return array
   *   An array of test cases, each which the following values:
   *   - The value array to pass to Flatten::transform().
   *   - The processed array returned by reference by Flatten::transform().
   */
  public static function flattenDataProvider() {
    return [
      [
        ['key1' => 'test1'],
        ['key1' => 'test1'],
      ],
      [
        ['key1' => ['value1' => 'test1']],
        ['key1.value1' => 'test1'],
      ],
      [
        ['key1' => ['value1' => 'test1'], 'key2' => 'test2'],
        ['key1.value1' => 'test1', 'key2' => 'test2'],
      ],
      [
        ['key1' => ['value1' => 'test1', 'value2' => 'test2']],
        ['key1.value1' => 'test1', 'key1.value2' => 'test2'],
      ],
      [
        ['key1' => ['value1' => 'test1', 'value2' => 'test2']],
        ['key1.value1' => 'test1', 'key1.value2' => 'test2'],
      ],
      [
        ['key1' => ['value1' => 'test1'], 'key2' => ['value2' => 'test2']],
        ['key1.value1' => 'test1', 'key2.value2' => 'test2'],
      ],
      [
        [
          'key1' => ['value1' => 'test1', 'value2' => ['subvalue1' => 'test2']],
          'key2' => ['value2' => 'test3'],
        ],
        [
          'key1.value1' => 'test1',
          'key2.value2' => 'test3',
          'key1.value2.subvalue1' => 'test2',
        ],
      ],
    ];
  }

}
