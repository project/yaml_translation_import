<?php

namespace Drupal\Tests\yaml_translation_import\Traits;

/**
 * Provides methods to check if translations exists or not.
 *
 * This trait is meant to be used only by test classes.
 */
trait TranslationCheckTrait {

  /**
   * Check if a translation exists.
   *
   * @param string $langcode
   *   The langcode we're searching.
   * @param string $source
   *   The translation source.
   */
  protected function checkIfTranslationExists(string $langcode, string $source): void {
    $translation_strings = \Drupal::service('locale.storage')->getTranslations([
      'language' => $langcode,
      'source' => $source,
    ]);

    $this->assertNotEmpty($translation_strings);
  }

  /**
   * Check if a translation NOT exists.
   *
   * @param string $langcode
   *   The langcode we're searching.
   * @param string $source
   *   The translation source.
   */
  protected function checkIfTranslationNotExists(string $langcode, string $source): void {
    $translation_strings = \Drupal::service('locale.storage')->getTranslations([
      'language' => $langcode,
      'source' => $source,
    ]);

    // It's possible that a translation string already exists, when it's
    // added in another language. When the translations strings array is not
    // empty, we check the translation is empty.
    if (!empty($translation_strings)) {
      $translation_strings = reset($translation_strings);

      $this->assertEquals($source, $translation_strings->source);
      $this->assertEmpty($translation_strings->getString());
    }
    else {
      $this->assertEmpty($translation_strings);
    }
  }

  /**
   * Check if a translation equals the given value.
   *
   * @param string $langcode
   *   The langcode we're searching.
   * @param string $source
   *   The translation source.
   * @param string $translation
   *   The actual translation.
   */
  protected function checkIfTranslationEquals(string $langcode, string $source, string $translation): void {
    $translation_strings = \Drupal::service('locale.storage')->getTranslations([
      'language' => $langcode,
      'source' => $source,
    ]);
    $translation_strings = reset($translation_strings);

    $this->assertNotEmpty($translation_strings);
    $this->assertEquals($source, $translation_strings->source);
    $this->assertEquals($translation, $translation_strings->getString());
  }

}
