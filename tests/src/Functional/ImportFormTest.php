<?php

namespace Drupal\Tests\yaml_translation_import\Functional;

use Drupal\Core\Url;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\yaml_translation_import\Traits\TranslationCheckTrait;

/**
 * Tests the yaml translation import from the UI.
 *
 * @group yaml_translation_import
 */
class ImportFormTest extends BrowserTestBase {

  use TranslationCheckTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['yaml_translation_import'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * User with admin rights.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * The relative path to the current module.
   *
   * @var string
   */
  protected $relativePathToModule;

  /**
   * A regular user.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $webUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->webUser = $this->createUser();
    $this->adminUser = $this->createUser(['access yaml import page']);

    ConfigurableLanguage::createFromLangcode('nl')->save();
    ConfigurableLanguage::createFromLangcode('fr')->save();

    $this->relativePathToModule = $this->container->get('extension.list.module')->getPath('yaml_translation_import');
  }

  /**
   * Tests the yaml translation import from the UI.
   */
  public function testYamlTranslationsSettingsForm() {
    $assert_session = $this->assertSession();
    $fr_language = ConfigurableLanguage::load('fr');
    $nl_language = ConfigurableLanguage::load('nl');

    // Unauthorized user should not have access.
    $this->drupalGet(Url::fromRoute('yaml_translation_import.yaml.import'));
    $this->assertSession()->statusCodeEquals(403);

    // Login as a regular user.
    $this->drupalLogin($this->webUser);

    // Unauthorized user should not have access.
    $this->drupalGet(Url::fromRoute('yaml_translation_import.yaml.import'));
    $this->assertSession()->statusCodeEquals(403);

    // Login as an admin user.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet(Url::fromRoute('yaml_translation_import.yaml.import'));
    $this->assertSession()->statusCodeEquals(200);

    // Check that the enabled languages are present.
    $assert_session->elementExists('css', 'input[name="languages[nl]"]');
    $assert_session->elementExists('css', 'input[name="languages[fr]"]');
    $assert_session->elementExists('css', 'input[name="languages[fr]"]');
    $assert_session->elementNotExists('css', 'input[name="languages[de]"]');

    $this->submitForm([
      'languages[nl]' => TRUE,
      'languages[fr]' => TRUE,
    ], 'Import');

    $assert_session->pageTextContains(sprintf('Import for "%s" was skipped because the translation file was not found', $fr_language->label()));
    $assert_session->pageTextContains(sprintf('Import for "%s" was skipped because the translation file was not found', $nl_language->label()));

    // Update the path so this time there will be an import file.
    $config = $this->config('yaml_translation_import.settings');
    $config->set('translation.directory', $this->relativePathToModule . '/tests/fixtures/translation_files/v1');
    $config->save();

    $this->drupalGet(Url::fromRoute('yaml_translation_import.yaml.import'));
    $this->submitForm([
      'languages[nl]' => TRUE,
      'languages[fr]' => TRUE,
    ], 'Import');
    $assert_session->pageTextContains(sprintf('Import for "%s" was skipped because the translation file was not found', $fr_language->label()));
    $assert_session->pageTextContains(sprintf('%s translations imported: %s added, %s updated, %s removed.', $nl_language->label(), 2, 0, 0));
    $this->checkIfTranslationEquals('nl', 'nl.test1', 'NL test 1');
    // Make sure invalid HTML is not imported.
    $this->checkIfTranslationNotExists('nl', 'nl.xss');
    // Make sure valid HTML is imported.
    $this->checkIfTranslationEquals('nl', 'nl.valid_html', '<a href=":url">Test</a>');
    $this->checkIfTranslationNotExists('nl', 'nl.test2');
    $this->checkIfTranslationNotExists('fr', 'fr.test1');

    // Update the path so this time there will be an import file.
    $config = $this->config('yaml_translation_import.settings');
    $config->set('translation.directory', $this->relativePathToModule . '/tests/fixtures/translation_files/v2');
    $config->save();

    $this->drupalGet(Url::fromRoute('yaml_translation_import.yaml.import'));
    $this->submitForm([
      'languages[nl]' => TRUE,
      'languages[fr]' => TRUE,
    ], 'Import');
    $assert_session->pageTextContains(sprintf('%s translations imported: %s added, %s updated, %s removed.', $fr_language->label(), 2, 0, 0));
    $assert_session->pageTextContains(sprintf('%s translations imported: %s added, %s updated, %s removed.', $nl_language->label(), 0, 0, 0));
    $this->checkIfTranslationEquals('fr', 'fr.test1', 'FR test 1');
    $this->checkIfTranslationEquals('fr', 'fr.test2', 'FR test 2');
    $this->checkIfTranslationNotExists('nl', 'nl.test2');

    // Update the path so this time there will be an import file.
    $config = $this->config('yaml_translation_import.settings');
    $config->set('translation.directory', $this->relativePathToModule . '/tests/fixtures/translation_files/v3');
    $config->save();

    $this->drupalGet(Url::fromRoute('yaml_translation_import.yaml.import'));
    $this->submitForm([
      'languages[nl]' => TRUE,
      'languages[fr]' => TRUE,
      'overwrite_customized' => TRUE,
    ], 'Import');
    $assert_session->pageTextContains(sprintf('%s translations imported: %s added, %s updated, %s removed.', $fr_language->label(), 0, 2, 0));
    $assert_session->pageTextContains(sprintf('%s translations imported: %s added, %s updated, %s removed.', $nl_language->label(), 1, 1, 0));
    $this->checkIfTranslationEquals('nl', 'nl.test1', 'NL new test 1');
    $this->checkIfTranslationEquals('nl', 'nl.test2', 'NL new test 2');
    $this->checkIfTranslationEquals('fr', 'fr.test1', 'FR new test 1');
    $this->checkIfTranslationEquals('fr', 'fr.test2', 'FR new test 2');
  }

}
