<?php

namespace Drupal\Tests\yaml_translation_import\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the yaml translation settings form.
 *
 * @group yaml_translation_import
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['yaml_translation_import'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * User with admin rights.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * A regular user.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $webUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->webUser = $this->createUser();
    $this->adminUser = $this->createUser(['access yaml settings page']);
  }

  /**
   * Tests the yaml translation import Settings form.
   */
  public function testYamlTranslationsSettingsForm() {
    // Check default config.
    $config = $this->config('yaml_translation_import.settings');
    $this->assertEquals('sites/default/translations', $config->get('translation.directory'));

    // Unauthorized user should not have access.
    $this->drupalGet(Url::fromRoute('yaml_translation_import.yaml.settings'));
    $this->assertSession()->statusCodeEquals(403);

    // Login as a regular user.
    $this->drupalLogin($this->webUser);

    // Unauthorized user should not have access.
    $this->drupalGet(Url::fromRoute('yaml_translation_import.yaml.settings'));
    $this->assertSession()->statusCodeEquals(403);

    // Login as an admin user.
    $this->drupalLogin($this->adminUser);

    // Change the config.
    $this->drupalGet(Url::fromRoute('yaml_translation_import.yaml.settings'));
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm(['directory' => 'sites/default/my_custom_dir'], 'Save configuration');

    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // Checking the configuration.
    $config = $this->config('yaml_translation_import.settings');
    $this->assertEquals('sites/default/my_custom_dir', $config->get('translation.directory'));
  }

}
