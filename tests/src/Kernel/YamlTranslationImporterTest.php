<?php

namespace Drupal\Tests\yaml_translation_import\Kernel;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\yaml_translation_import\Traits\TranslationCheckTrait;

/**
 * Tests the 'yaml_translation_import.import.yaml_translation' service.
 *
 * @group yaml_translation_import
 */
class YamlTranslationImporterTest extends KernelTestBase {

  use TranslationCheckTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['yaml_translation_import', 'locale', 'language'];

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The absolute path to the current module.
   *
   * @var string
   */
  protected $absolutePathToModule;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('locale', ['locales_source', 'locales_target']);
    $this->installConfig('yaml_translation_import');

    ConfigurableLanguage::createFromLangcode('nl')->save();
    ConfigurableLanguage::createFromLangcode('fr')->save();

    $this->absolutePathToModule = $this->container->getParameter('app.root') . '/' . $this->container->get('extension.list.module')->getPath('yaml_translation_import');
    $this->messenger = $this->container->get('messenger');
  }

  /**
   * Test adding translations.
   */
  public function testAddingTranslations() {
    $nl_language = ConfigurableLanguage::load('nl');
    $fr_language = ConfigurableLanguage::load('fr');

    /** @var \Drupal\yaml_translation_import\Import\YamlTranslationImporter $yaml_translation_importer */
    $yaml_translation_importer = $this->container->get('yaml_translation_import.import.yaml_translation');
    $yaml_translation_importer->execute([
      $nl_language->id(),
      $fr_language->id(),
    ]);
    $messages = $this->messenger->messagesByType(MessengerInterface::TYPE_STATUS);
    $this->messenger->deleteAll();
    $this->assertEquals(sprintf('Import for "%s" was skipped because the translation file was not found.', $nl_language->label()), $messages[0]);
    $this->assertEquals(sprintf('Import for "%s" was skipped because the translation file was not found.', $fr_language->label()), $messages[1]);

    $yaml_translation_importer->setTranslationDirectory($this->absolutePathToModule . '/tests/fixtures/translation_files/v1');
    $yaml_translation_importer->execute([
      $nl_language->id(),
      $fr_language->id(),
    ]);
    $messages = $this->messenger->messagesByType(MessengerInterface::TYPE_STATUS);
    $this->messenger->deleteAll();
    $this->assertEquals(sprintf('%s translations imported: %s added, %s updated, %s removed.', $nl_language->label(), 2, 0, 0), $messages[0]);
    $this->assertEquals(sprintf('Import for "%s" was skipped because the translation file was not found.', $fr_language->label()), $messages[1]);
    $this->checkIfTranslationEquals('nl', 'nl.test1', 'NL test 1');
    $this->checkIfTranslationNotExists('nl', 'nl.xss');
    $this->checkIfTranslationEquals('nl', 'nl.valid_html', '<a href=":url">Test</a>');
    $this->checkIfTranslationNotExists('nl', 'nl.test2');
    $this->checkIfTranslationNotExists('fr', 'fr.test1');

    $yaml_translation_importer->setTranslationDirectory($this->absolutePathToModule . '/tests/fixtures/translation_files/v2');
    $yaml_translation_importer->execute([
      $nl_language->id(),
      $fr_language->id(),
    ]);
    $messages = $this->messenger->messagesByType(MessengerInterface::TYPE_STATUS);
    $this->messenger->deleteAll();
    $this->assertEquals(sprintf('%s translations imported: %s added, %s updated, %s removed.', $nl_language->label(), 0, 0, 0), $messages[0]);
    $this->assertEquals(sprintf('%s translations imported: %s added, %s updated, %s removed.', $fr_language->label(), 2, 0, 0), $messages[1]);
    $this->checkIfTranslationEquals('fr', 'fr.test1', 'FR test 1');
    $this->checkIfTranslationEquals('fr', 'fr.test2', 'FR test 2');
    $this->checkIfTranslationNotExists('nl', 'nl.test2');

    $yaml_translation_importer->setTranslationDirectory($this->absolutePathToModule . '/tests/fixtures/translation_files/v3');
    $yaml_translation_importer->execute([
      $nl_language->id(),
      $fr_language->id(),
    ], ['overwrite_options' => ['customized' => TRUE]]);
    $messages = $this->messenger->messagesByType(MessengerInterface::TYPE_STATUS);
    $this->messenger->deleteAll();
    $this->assertEquals(sprintf('%s translations imported: %s added, %s updated, %s removed.', $nl_language->label(), 1, 1, 0), $messages[0]);
    $this->assertEquals(sprintf('%s translations imported: %s added, %s updated, %s removed.', $fr_language->label(), 0, 2, 0), $messages[1]);
    $this->checkIfTranslationEquals('nl', 'nl.test1', 'NL new test 1');
    $this->checkIfTranslationEquals('nl', 'nl.test2', 'NL new test 2');
    $this->checkIfTranslationEquals('fr', 'fr.test1', 'FR new test 1');
    $this->checkIfTranslationEquals('fr', 'fr.test2', 'FR new test 2');
  }

}
