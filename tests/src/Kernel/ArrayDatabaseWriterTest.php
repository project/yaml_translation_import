<?php

namespace Drupal\Tests\yaml_translation_import\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\yaml_translation_import\Traits\TranslationCheckTrait;

/**
 * Tests the 'yaml_translation_import.writer.array_database_writer' service.
 *
 * @group yaml_translation_import
 */
class ArrayDatabaseWriterTest extends KernelTestBase {

  use TranslationCheckTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['yaml_translation_import', 'locale', 'language'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('locale', ['locales_source', 'locales_target']);

    ConfigurableLanguage::createFromLangcode('nl')->save();
    ConfigurableLanguage::createFromLangcode('fr')->save();
  }

  /**
   * Test adding translations.
   */
  public function testAddingTranslations() {
    $nl_language = ConfigurableLanguage::load('nl');
    $fr_language = ConfigurableLanguage::load('fr');

    /** @var \Drupal\yaml_translation_import\Writer\ArrayDatabaseWriter $arrayDatabaseWriterService */
    $arrayDatabaseWriterService = \Drupal::service('yaml_translation_import.writer.array_database_writer');

    // Test default report values.
    $report = $arrayDatabaseWriterService->getReport();
    $this->assertEquals(0, $report['additions']);
    $this->assertEquals(0, $report['updates']);
    $this->assertEquals(0, $report['deletes']);
    $this->assertEquals(0, $report['skips']);

    $arrayDatabaseWriterService->setLangcode($nl_language->id());
    $arrayDatabaseWriterService->writeItems([
      'test.translation_1' => 'test 1',
      'test.translation_2' => 'test 2',
    ]);
    $this->checkIfTranslationEquals($nl_language->id(), 'test.translation_1', 'test 1');
    $this->checkIfTranslationEquals($nl_language->id(), 'test.translation_2', 'test 2');
    $this->checkIfTranslationNotExists($fr_language->id(), 'test.translation_1');
    $this->checkIfTranslationNotExists($fr_language->id(), 'test.translation_2');
    $this->checkIfTranslationNotExists($nl_language->id(), 'test.translation_3');
    $this->checkIfTranslationNotExists($nl_language->id(), 'test.translation_4');

    $arrayDatabaseWriterService->writeItems([
      // Make sure unsafe translations are not added.
      'test.translation_3' => '<script>alert(\'test\');</script>',
      // Safe HTML can be added.
      'test.translation_4' => '<a href=":url">Test 4</a>',
    ]);
    $this->checkIfTranslationNotExists($nl_language->id(), 'test.translation_3');
    $this->checkIfTranslationEquals($nl_language->id(), 'test.translation_4', '<a href=":url">Test 4</a>');

    // Test report values after adding translations.
    $report = $arrayDatabaseWriterService->getReport();
    $this->assertEquals(3, $report['additions']);
    $this->assertEquals(0, $report['updates']);
    $this->assertEquals(0, $report['deletes']);
    $this->assertEquals(1, $report['skips']);

    // Test report values after reset.
    $arrayDatabaseWriterService->resetReport();
    $report = $arrayDatabaseWriterService->getReport();
    $this->assertEquals(0, $report['additions']);
    $this->assertEquals(0, $report['updates']);
    $this->assertEquals(0, $report['deletes']);
    $this->assertEquals(0, $report['skips']);

    // Adding an existing value is only allowed when this is explicitly
    // configured using the setOptions method.
    $arrayDatabaseWriterService->writeItems([
      'test.translation_1' => 'test 1 - new value',
    ]);
    $this->checkIfTranslationEquals($nl_language->id(), 'test.translation_1', 'test 1');

    $report = $arrayDatabaseWriterService->getReport();
    $this->assertEquals(0, $report['additions']);
    $this->assertEquals(0, $report['updates']);
    $this->assertEquals(0, $report['deletes']);
    $this->assertEquals(0, $report['skips']);
    $arrayDatabaseWriterService->resetReport();

    $arrayDatabaseWriterService->setOptions(['overwrite_options' => ['customized' => TRUE]]);
    $arrayDatabaseWriterService->writeItems([
      'test.translation_1' => 'test 1 - new value',
    ]);
    $this->checkIfTranslationEquals($nl_language->id(), 'test.translation_1', 'test 1 - new value');

    $report = $arrayDatabaseWriterService->getReport();
    $this->assertEquals(0, $report['additions']);
    $this->assertEquals(1, $report['updates']);
    $this->assertEquals(0, $report['deletes']);
    $this->assertEquals(0, $report['skips']);
    $arrayDatabaseWriterService->resetReport();

    $arrayDatabaseWriterService->setOptions(['overwrite_options' => ['not_customized' => TRUE]]);
    $arrayDatabaseWriterService->writeItems([
      'test.translation_1' => 'test 1 - newer value',
    ]);
    $this->checkIfTranslationEquals($nl_language->id(), 'test.translation_1', 'test 1 - new value');

    $report = $arrayDatabaseWriterService->getReport();
    $this->assertEquals(0, $report['additions']);
    $this->assertEquals(0, $report['updates']);
    $this->assertEquals(0, $report['deletes']);
    $this->assertEquals(0, $report['skips']);
    $arrayDatabaseWriterService->resetReport();
  }

}
