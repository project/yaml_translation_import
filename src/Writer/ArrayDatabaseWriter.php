<?php

namespace Drupal\yaml_translation_import\Writer;

use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\locale\StringStorageInterface;
use Drupal\locale\TranslationString;

/**
 * Class that writes the translations to the database.
 */
class ArrayDatabaseWriter {

  /**
   * An associative array indicating what data should be overwritten, if any.
   *
   * Elements of the array:
   * - override_options
   *   - not_customized: boolean indicating that not customized strings should
   *     be overwritten.
   *   - customized: boolean indicating that customized strings should be
   *     overwritten.
   * - customized: the strings being imported should be saved as customized.
   *     One of LOCALE_CUSTOMIZED or LOCALE_NOT_CUSTOMIZED.
   *
   * @var array
   */
  private $options;

  /**
   * Language code of the language being written to the database.
   *
   * @var string
   */
  private $langcode;

  /**
   * Associative array summarizing the number of changes done.
   *
   * Keys for the array:
   *  - additions: number of source strings newly added
   *  - updates: number of translations updated
   *  - deletes: number of translations deleted
   *  - skips: number of strings skipped due to disallowed HTML.
   *
   * @var array
   */
  private $report;

  /**
   * The locale storage.
   *
   * @var \Drupal\locale\StringStorageInterface
   */
  private $localeStorage;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * ArrayDatabaseWriter constructor.
   *
   * @param \Drupal\locale\StringStorageInterface $localeStorage
   *   The locale storage.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerChannelFactory
   *   The logger channel factory.
   */
  public function __construct(StringStorageInterface $localeStorage, LoggerChannelFactory $loggerChannelFactory) {
    $this->setReport();
    $this->setOptions([]);
    $this->localeStorage = $localeStorage;
    $this->logger = $loggerChannelFactory->get('locale');
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode(): string {
    return $this->langcode;
  }

  /**
   * {@inheritdoc}
   */
  public function setLangcode(string $langcode): void {
    $this->langcode = $langcode;
  }

  /**
   * Get the report of the write operations.
   */
  public function getReport(): array {
    return $this->report;
  }

  /**
   * Set the report array of write operations.
   *
   * @param array $report
   *   Associative array with result information.
   */
  public function setReport(array $report = []): void {
    $report += [
      'additions' => 0,
      'updates' => 0,
      'deletes' => 0,
      'skips' => 0,
      'strings' => [],
    ];
    $this->report = $report;
  }

  /**
   * Reset the report to the default values.
   */
  public function resetReport(): void {
    $this->report = [
      'additions' => 0,
      'updates' => 0,
      'deletes' => 0,
      'skips' => 0,
      'strings' => [],
    ];
  }

  /**
   * Get the options used by the writer.
   */
  public function getOptions(): array {
    return $this->options;
  }

  /**
   * Set the options used by the writers.
   *
   * @param array $options
   *   The options.
   */
  public function setOptions(array $options): void {
    if (!isset($options['overwrite_options'])) {
      $options['overwrite_options'] = [];
    }
    $options['overwrite_options'] += [
      'not_customized' => FALSE,
      'customized' => FALSE,
    ];
    $options += [
      'customized' => LOCALE_CUSTOMIZED,
    ];
    $this->options = $options;
  }

  /**
   * Write the given items to the database.
   *
   * @param array $translations
   *   The translations.
   * @param string $context
   *   The context.
   *
   * @throws \Drupal\locale\StringStorageException
   */
  public function writeItems(array $translations, string $context = ''): void {
    foreach ($translations as $source => $translation) {
      $this->writeItem($source, $translation, $context);
    }
  }

  /**
   * Write a single item to the database.
   *
   * @param string $source
   *   The source string.
   * @param string $translation
   *   The translation.
   * @param string $context
   *   The context.
   *
   * @throws \Drupal\locale\StringStorageException
   */
  public function writeItem(string $source, string $translation, string $context = ''): void {
    $this->importString($source, $translation, $context);
  }

  /**
   * Imports a string to the database.
   *
   * Function is heavily inspired by the PoDatabaseWriter().
   *
   * @param string $source
   *   The source string.
   * @param string $translation
   *   The translation.
   * @param string $context
   *   The context.
   *
   * @return int|null
   *   The string translation ID.
   *
   * @throws \Drupal\locale\StringStorageException
   *
   * @see \Drupal\locale\PoDatabaseWriter::importString()
   */
  private function importString(string $source, string $translation, string $context = ''): ?int {
    $overwrite_options = $this->options['overwrite_options'];
    $customized = $this->options['customized'];

    // Look up the source string and any existing translation.
    $strings = $this->localeStorage->getTranslations([
      'language' => $this->langcode,
      'source' => $source,
      'context' => $context,
    ]);
    $string = reset($strings);

    if (!empty($translation)) {
      // Skip this string unless it passes a check for dangerous code.
      if (!locale_string_is_safe($translation)) {
        $this->logger->error('Import of string "%string" was skipped because of disallowed or malformed HTML.', ['%string' => $translation]);
        $this->report['skips']++;
        return 0;
      }
      elseif ($string instanceof TranslationString) {
        $string->setString($translation);
        if ($string->isNew()) {
          // No translation in this language.
          $string->setValues([
            'language' => $this->langcode,
            'customized' => $customized,
          ]);
          $string->save();
          $this->report['additions']++;
        }
        elseif ($overwrite_options[$string->customized ? 'customized' : 'not_customized']) {
          // Translation exists, only overwrite if instructed.
          $string->customized = $customized;
          $string->save();
          $this->report['updates']++;
        }
        $this->report['strings'][] = $string->getId();
        return $string->getId();
      }
      else {
        // No such source string in the database yet.
        $string = $this->localeStorage
          ->createString(['source' => $source, 'context' => $context])
          ->save();
        $this->localeStorage->createTranslation([
          'lid' => $string->getId(),
          'language' => $this->langcode,
          'translation' => $translation,
          'customized' => $customized,
        ])->save();

        $this->report['additions']++;
        $this->report['strings'][] = $string->getId();
        return $string->lid;
      }
    }
    elseif ($string instanceof TranslationString && !$string->isNew() && $overwrite_options[$string->customized ? 'customized' : 'not_customized']) {
      // Empty translation, remove existing if instructed.
      $string->delete();
      $this->report['deletes']++;
      $this->report['strings'][] = $string->getId();
      return $string->getId();
    }

    return NULL;
  }

}
