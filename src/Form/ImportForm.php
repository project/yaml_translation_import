<?php

namespace Drupal\yaml_translation_import\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\yaml_translation_import\Import\YamlTranslationImporter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The form that imports the yaml translations.
 *
 * @package Drupal\yaml_translation_import\Form
 */
class ImportForm extends FormBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The yaml translation importer.
   *
   * @var \Drupal\yaml_translation_import\Import\YamlTranslationImporter
   */
  protected $yamlTranslationImporter;

  /**
   * ImportForm constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\yaml_translation_import\Import\YamlTranslationImporter $yaml_translation_importer
   *   The yaml translation importer.
   */
  public function __construct(LanguageManagerInterface $language_manager, YamlTranslationImporter $yaml_translation_importer) {
    $this->languageManager = $language_manager;
    $this->yamlTranslationImporter = $yaml_translation_importer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager'),
      $container->get('yaml_translation_import.import.yaml_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'yaml_translation_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['languages'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Language'),
      '#required' => TRUE,
      '#options' => $this->getLanguageOptions(),
    ];
    $form['options_markup'] = [
      '#markup' => new FormattableMarkup('<label>@title</label>', ['@title' => $this->t('Options')]),
    ];
    $form['overwrite_customized'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Overwrite customized translations'),
    ];
    $form['overwrite_not_customized'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Overwrite non-customized translations'),
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
    ];

    return $form;
  }

  /**
   * Get the possible language options.
   *
   * @return array
   *   An array containing the languages that can be used as checkbox options.
   */
  protected function getLanguageOptions(): array {
    $language_options = [];
    $languages = $this->languageManager->getLanguages();

    foreach ($languages as $language) {
      $language_options[$language->getId()] = $language->getName();
    }

    return $language_options;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $import_options = [];
    $import_options['overwrite_options']['customized'] = $form_state->getValue('overwrite_customized') === 1;
    $import_options['overwrite_options']['not_customized'] = $form_state->getValue('overwrite_not_customized') === 1;
    $langcodes = array_filter($form_state->getValue('languages'));

    $this->yamlTranslationImporter->execute($langcodes, $import_options);
  }

}
