<?php

namespace Drupal\yaml_translation_import\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The settings form.
 *
 * @package Drupal\yaml_translation_import\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'yaml_translation_import_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['yaml_translation_import.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('yaml_translation_import.settings');
    $form = parent::buildForm($form, $form_state);

    $form['directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Translation directory'),
      '#description' => $this->t('Directory where the translations are saved.'),
      '#default_value' => $config->get('translation.directory'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('yaml_translation_import.settings');

    $config->set('translation.directory', $form_state->getValue('directory'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
