<?php

namespace Drupal\yaml_translation_import;

use Drupal\Core\Language\LanguageInterface;
use Drupal\yaml_translation_import\Utility\Flatten;
use Drupal\yaml_translation_import\Writer\ArrayDatabaseWriter;
use Symfony\Component\Yaml\Yaml;

/**
 * Class that imports the given yaml file.
 */
class YamlImporter {

  /**
   * The array database writer.
   *
   * @var \Drupal\yaml_translation_import\Writer\ArrayDatabaseWriter
   */
  protected $arrayDatabaseWriter;

  /**
   * YamlImporter constructor.
   *
   * @param \Drupal\yaml_translation_import\Writer\ArrayDatabaseWriter $arrayDatabaseWriter
   *   The array database writer.
   */
  public function __construct(ArrayDatabaseWriter $arrayDatabaseWriter) {
    $this->arrayDatabaseWriter = $arrayDatabaseWriter;
  }

  /**
   * Imports the given file for the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language.
   * @param string $resource
   *   The path to the file.
   * @param array $options
   *   The options used by the yaml writer.
   *
   * @return array
   *   The report array of write operations.
   *
   * @throws \Drupal\locale\StringStorageException
   */
  public function import(LanguageInterface $language, string $resource, array $options = []): array {
    $translations = Yaml::parseFile($resource);
    Flatten::transform($translations);

    $this->arrayDatabaseWriter->setLangcode($language->getId());
    $this->arrayDatabaseWriter->setOptions($options);
    $this->arrayDatabaseWriter->writeItems($translations);
    // Report back with an array of status information.
    $report = $this->arrayDatabaseWriter->getReport();
    $this->arrayDatabaseWriter->resetReport();

    _locale_refresh_translations([$language->getId()]);
    _locale_refresh_configuration([$language->getId()], []);

    return $report;
  }

}
