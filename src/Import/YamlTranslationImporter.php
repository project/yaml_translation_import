<?php

namespace Drupal\yaml_translation_import\Import;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\yaml_translation_import\Exception\LanguageDoesNotExistException;
use Drupal\yaml_translation_import\YamlImporter;
use Symfony\Component\Console\Style\StyleInterface;

/**
 * Class Import.
 */
class YamlTranslationImporter {

  use StringTranslationTrait;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The translation directory.
   *
   * @var string
   */
  protected $translationDirectory;

  /**
   * The drush messenger.
   *
   * @var \Drush\Style\DrushStyle
   */
  protected $drushMessenger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The YAML importer.
   *
   * @var \Drupal\yaml_translation_import\YamlImporter
   */
  protected $yamlImporter;

  /**
   * A boolean indicating that the import is run from cli or not.
   *
   * @var bool
   */
  protected $cli = FALSE;

  /**
   * Import constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param string $root
   *   The application root.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\yaml_translation_import\YamlImporter $yamlImporter
   *   The YAML Importer.
   */
  public function __construct(ConfigFactoryInterface $configFactory, LanguageManagerInterface $languageManager, string $root, MessengerInterface $messenger, YamlImporter $yamlImporter) {
    $this->languageManager = $languageManager;
    $this->setTranslationDirectory($root . '/' . trim($configFactory->get('yaml_translation_import.settings')->get('translation.directory'), '/'));
    $this->messenger = $messenger;
    $this->yamlImporter = $yamlImporter;
  }

  /**
   * Execute the import command.
   *
   * When no langcode parameter is given, we import all the existing languages.
   *
   * @param array|null $langcodes
   *   The language IDs.
   * @param array $options
   *   The options used by the yaml writer.
   *
   * @throws \Drupal\locale\StringStorageException
   */
  public function execute(array $langcodes = NULL, array $options = []): void {
    $languages = $this->getImportLanguages($langcodes);

    foreach ($languages as $language) {
      $resource = $this->getTranslationFilePath($language->getId());
      if (!file_exists($resource)) {
        $this->printMessage($this->t('Import for "@language" was skipped because the translation file was not found.', [
          '@language' => $language->getName(),
        ]));
        continue;
      }

      $report = $this->yamlImporter->import($language, $resource, $options);

      $this->printMessage($this->t('@language translations imported: @number added, @update updated, @delete removed.', [
        '@language' => $language->getName(),
        '@number' => $report['additions'],
        '@update' => $report['updates'],
        '@delete' => $report['deletes'],
      ]));
    }
  }

  /**
   * Returns the languages we are going to import.
   *
   * When no language is given, we return all languages.
   *
   * @param array|null $langcodes
   *   The language ID.
   *
   * @return \Drupal\Core\Language\LanguageInterface[]
   *   An array of languages.
   *
   * @throws \Drupal\yaml_translation_import\Exception\LanguageDoesNotExistException
   */
  protected function getImportLanguages(array $langcodes = NULL): array {
    $languages = [];

    if (!empty($langcodes)) {
      foreach ($langcodes as $langcode) {
        $language = $this->languageManager->getLanguage($langcode);

        if ($language === NULL) {
          throw new LanguageDoesNotExistException(t('language "@id" does not exist.', ['@id' => $langcode]));
        }

        $languages[] = $language;
      }
    }
    else {
      $languages = $this->languageManager->getLanguages();
    }

    return $languages;
  }

  /**
   * Returns the translation file path for the give langcode.
   *
   * @param string $langcode
   *   The language ID.
   *
   * @return string
   *   The translation file path.
   */
  protected function getTranslationFilePath(string $langcode): string {
    return $this->getTranslationDirectory() . '/translations.' . $langcode . '.yml';
  }

  /**
   * Override the translation directory.
   *
   * @param string $translationDirectory
   *   The translation directory.
   *
   * @return $this
   *   The called class.
   */
  public function setTranslationDirectory(string $translationDirectory): YamlTranslationImporter {
    $this->translationDirectory = $translationDirectory;
    return $this;
  }

  /**
   * Get the translation directory.
   *
   * @return string
   *   The translation directory.
   */
  protected function getTranslationDirectory(): string {
    return $this->translationDirectory;
  }

  /**
   * Print a message.
   *
   * @param string $message
   *   A string that should be shown to the user.
   */
  protected function printMessage(string $message): void {
    if ($this->isCli()) {
      $this->drushMessenger->note($message);
    }
    else {
      $this->messenger->addStatus($message);
    }
  }

  /**
   * Check if the import is running from CLI.
   *
   * @return bool
   *   A bool indicating that the import is running from CLI.
   */
  public function isCli(): bool {
    return $this->cli;
  }

  /**
   * Set CLI.
   *
   * @param bool $cli
   *   A boolean indicating that the import is running from cli.
   *
   * @return $this
   *   The current class.
   */
  public function setCli(bool $cli): self {
    $this->cli = $cli;
    return $this;
  }

  /**
   * Set the drush messenger.
   *
   * @param \Symfony\Component\Console\Style\StyleInterface $drushMessenger
   *   The drush messenger that prints messages to the cli.
   *
   * @return $this
   *   The current class.
   */
  public function setDrushMessenger(StyleInterface $drushMessenger): self {
    $this->drushMessenger = $drushMessenger;
    return $this;
  }

}
