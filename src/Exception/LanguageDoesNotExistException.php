<?php

namespace Drupal\yaml_translation_import\Exception;

/**
 * Exception thrown when trying to load a language that does not exist.
 */
class LanguageDoesNotExistException extends \RuntimeException {}
