<?php

namespace Drupal\yaml_translation_import\Command;

use Drupal\yaml_translation_import\Import\YamlTranslationImporter;
use Drush\Commands\DrushCommands;

/**
 * Class Import.
 */
class YamlTranslationImportCommands extends DrushCommands {

  /**
   * The yaml translation importer.
   *
   * @var \Drupal\yaml_translation_import\Import\YamlTranslationImporter
   */
  protected $yamlTranslationImporter;

  /**
   * Import constructor.
   *
   * @param \Drupal\yaml_translation_import\Import\YamlTranslationImporter $yaml_translation_importer
   *   The yaml translation importer.
   */
  public function __construct(YamlTranslationImporter $yaml_translation_importer) {
    parent::__construct();
    $this->yamlTranslationImporter = $yaml_translation_importer;
  }

  /**
   * Import YAML translations.
   *
   * @param string $language
   *   The language you want to import. All when empty.
   * @param array $options
   *   An array of options whose values come from cli, aliases, config, etc.
   *
   * @option overwrite-customized
   *   Overwrite customized translations.
   * @option overwrite-not-customized
   *   Overwrite non customized translations.
   * @validate-module-enabled yaml_translation_import
   *
   * @command yaml:translation-import
   * @aliases yti,yaml-translation-import
   *
   * @throws \Drupal\locale\StringStorageException
   */
  public function yamlTranslationImport(
    string $language = NULL,
    array $options = [
      'overwrite-customized' => NULL,
      'overwrite-not-customized' => NULL,
    ],
  ): void {
    $import_options = [];

    if ($options['overwrite-customized'] === TRUE) {
      $import_options['overwrite_options']['customized'] = TRUE;
    }
    if ($options['overwrite-not-customized'] === TRUE) {
      $import_options['overwrite_options']['not_customized'] = TRUE;
    }

    $langcodes = [];
    if ($language !== NULL) {
      $langcodes[] = $language;
    }

    $this->yamlTranslationImporter->setCli(TRUE);
    $this->yamlTranslationImporter->setDrushMessenger($this->io());
    $this->yamlTranslationImporter->execute($langcodes, $import_options);
  }

}
