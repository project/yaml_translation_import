<?php

namespace Drupal\yaml_translation_import\Utility;

/**
 * Class Flatten.
 *
 * Helper class that converts a nested array of translations.
 *
 * @package Drupal\yaml_translation_import\Utility
 */
class Flatten {

  /**
   * Flattens an nested array of translations.
   *
   * The scheme used is:
   *   'key' => array('key2' => array('key3' => 'value'))
   * Becomes:
   *   'key.key2.key3' => 'value'
   *
   * This function takes an array by reference and will modify it.
   *
   * @param array $messages
   *   The array that will be flattened.
   * @param array|null $subnode
   *   Current subnode being parsed, used internally for recursive calls.
   * @param string|null $path
   *   Current path being parsed, used internally for recursive calls.
   */
  public static function transform(array &$messages, array $subnode = NULL, string $path = NULL): void {
    if (NULL === $subnode) {
      $subnode = &$messages;
    }
    foreach ($subnode as $key => $value) {
      if (is_array($value)) {
        $nodePath = (string) ($path ? $path . '.' . $key : $key);
        self::transform($messages, $value, $nodePath);
        if (NULL === $path) {
          unset($messages[$key]);
        }
      }
      elseif (NULL !== $path) {
        $messages[$path . '.' . $key] = $value;
      }
    }
  }

}
