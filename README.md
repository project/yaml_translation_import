# Yaml Translation Import

Developer module that makes it possible to import YAML Translation files.

## Introduction

The initial idea of this module was to provide translations as the 
[Symfony Translation](https://symfony.com/doc/current/components/translation/usage.html) 
component.

This workflow requires that "Enable interface translation" for your default 
language is enabled.

The translations provided in your custom code will be keyword messages, 
e.g. `drupal.is.great`. Those keywords will act as an ID. Since this 
keyword message is no actual translation, but a keyword that conveys the idea
of the message, your keyword doesn't have to be changed, every time you want
to update a label in the default language.

Translations can be added in a .yml file. By default those translation files
are added in sites/default/translations. Translations are group in a file per
language. The filename follows the this convention: 
`translations.[language_id].yml`.
E.g. When you want to add English translations, the filename will be 
`translations.en.yml`.

An example of a translation file:
```yaml
drupal:
  is:
    great: 'Drupal is great!'
    amazing: 'Drupal is amazing!'
  uses:
    modules: 'Drupal uses modules'
```

The advantage of using YAML as a translation file format is that labels that 
belong together can be grouped. E.g. All labels related to "Drupal" can start
with drupal. In your YAML file, all Drupal related translations are grouped
together.

Another advantage of using a file, is that you can provide default labels.
This module provides a backend screen on 
`/admin/config/regional/translate/yaml/import`
where you can import your newly added translations or even update old 
translations.
This module can also be part of you deployments by using the drush command 
`drush yti`.

## REQUIREMENTS
None

## INSTALLATION
Install the "YAML Translation Import" module as
you would normally install a contributed Drupal module.

Visit https://www.drupal.org/node/1897420 for further
information.

## CONFIGURATION
You can configure the directory where the translation files are saved. By 
default this will be "sites/default/translations". If you want to edit 
the directory, you can do this in the UI on 
"/admin/config/regional/translate/yaml/settings".

## MAINTAINERS
Current maintainers:
* Jeroen Tubex - https://www.drupal.org/u/jeroent
